'use strict';

module.exports = getquerystring;

function getquerystring(name) {
    // TODO
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
    var r = decodeURI(window.location.href.split("?")[1]).substr(0).match(reg); //匹配目标参数
    if (r != null) return unescape(r[2]); return null; //返回参数值
}
