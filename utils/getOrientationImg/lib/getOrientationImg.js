'use strict';
const lrz = require("lrz")
const semver = require("semver")
let str = navigator.userAgent.toLowerCase();
let ver = str.match(/cpu iphone os (.*?) like mac os/);
const iosVersion = ver[1].replace(/_/g, ".")
function getOrientation(file) {
    return new Promise((resolve) => {
        var reader = new window.FileReader();
        reader.onload = function (e) {

            var view = new window.DataView(e.target.result);
            if (view.getUint16(0, false) != 0xFFD8) {
                return resolve(-2);
            }
            var length = view.byteLength, offset = 2;
            while (offset < length) {
                var marker = view.getUint16(offset, false);
                offset += 2;
                if (marker == 0xFFE1) {
                    if (view.getUint32(offset += 2, false) != 0x45786966) {
                        return resolve(-1);
                    }
                    var little = view.getUint16(offset += 6, false) == 0x4949;
                    offset += view.getUint32(offset + 4, little);
                    var tags = view.getUint16(offset, little);
                    offset += 2;
                    for (var i = 0; i < tags; i++) {
                        if (view.getUint16(offset + (i * 12), little) == 0x0112) {
                            return resolve(view.getUint16(offset + (i * 12) + 8, little));
                        }

                    }
                } else if ((marker & 0xFF00) != 0xFF00) {
                    break;
                } else {
                    offset += view.getUint16(offset, false);
                }
            }
            return resolve(-1);
        };
        reader.readAsArrayBuffer(file);
    })

}

/**
 * 
 * @param {File} file 图片对象
 * @param {*} isCompass 是否压缩
 * @returns 
 */
module.exports = async function (file, isCompass) {
    //声明js的文件流
    let Orientation = await getOrientation(file)
    console.log(Orientation)
    var reader = new FileReader();
    if (file) {
        return new Promise((resolve) => {
            let img = new Image()
            //通过文件流将文件转换成Base64字符串
            reader.readAsDataURL(file);
            //转换成功后
            reader.onloadend = function () {
                //输出结果    
                //resolve(reader.result);
                img.src = reader.result
                img.onload = async function () {
                    let canvas = document.createElement("canvas");
                    let ctx = canvas.getContext("2d");
                    let imgWidth = canvas.width = this.width;
                    let imgHeight = canvas.height = this.height;
                    ctx.fillStyle = "#fff";
                    ctx.fillRect(0, 0, imgWidth, imgHeight);
                    //如果方向角不为1，都需要进行旋转 ios13.4以上图片旋转已被修复
                    if (Orientation && Orientation != 1 && semver.lt(iosVersion, '13.4.0')) {
                        switch (Orientation) {
                            case 6:     // 旋转90度
                                canvas.width = this.height;
                                canvas.height = this.width;
                                ctx.rotate(Math.PI / 2);
                                // (0,-imgHeight) 从旋转原理图那里获得的起始点
                                ctx.drawImage(this, 0, -imgHeight, imgWidth, imgHeight);
                                break;
                            case 3:     // 旋转180度
                                ctx.rotate(Math.PI);
                                ctx.drawImage(this, -imgWidth, -imgHeight, imgWidth, imgHeight);
                                break;
                            case 8:     // 旋转-90度
                                canvas.width = imgHeight;
                                canvas.height = imgWidth;
                                ctx.rotate(3 * Math.PI / 2);
                                ctx.drawImage(this, -imgWidth, 0, imgWidth, imgHeight);
                                break;
                            default:
                                ctx.drawImage(this, 0, 0, imgWidth, imgHeight);
                                break
                        }
                    } else {
                        ctx.drawImage(this, 0, 0, imgWidth, imgHeight);
                    }
                    ctx.drawImage(this, 0, 0, imgWidth, imgHeight);
                    if (isCompass) {
                        let rst = await lrz(file)
                        file = rst.file
                    }
                    resolve(canvas.toDataURL(file.type, 1))
                }
            }
        })

    }
    return null
}
