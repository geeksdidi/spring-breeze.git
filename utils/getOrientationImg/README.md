# `@spring-breeze/getOrientationImg`

> 解决ios拍照旋转问题

## Usage

```
const getorientationimg = require('@spring-breeze/getOrientationImg');
/**
 * 
 * @param {File} file 图片对象
 * @param {*} isCompass 是否压缩
 * @returns Base64
 */
 getorientationimg(File,1).then(base64=>{
     console.log(base64)
 })
```


