# 

## import-local的使用

### 作用
> 当我们本地node_modules存在一个脚手架命令，同时全局node_modules中也存在这个脚手架命令的时候，优先选用**本地node_modules**中的版本

> 进入core文件下执行:  npm i import-local -S  
> 安装npmlog: npm install npmlog -S  

bin/index.js

```
#!/usr/bin/env node
const importLocal = require("import-local")
if(importLocal(__filename)){
    require('npmlog').info("cli","正在使用本地的cli")
}else{
    require('../lib/core')()
}
```

lib/core.js

```
module.exports = core;
function core(){
    console.log('start')
}
```

执行spring-breeze-cli

```
PS F:\spring-breeze\core\core> spring-breeze-cli
start
```

## 检测版本号
```
const pkg = require("../package.json")
//检查版本号
function checkVersion(){
    console.log(pkg.version)
}
```

## npmlog再封装

> 在utils下新建模块log:     
lerna create log utils  
这样做是为了可以自定义log

utils/log下的package.json如下:

```
{
  "name": "@spring-breeze/log",
  "version": "0.0.5",
  "description": "> TODO: description",
  "author": "",
  "homepage": "",
  "license": "ISC",
  "main": "lib/log.js",
  "directories": {
    "lib": "lib",
    "test": "__tests__"
  },
  "files": [
    "lib"
  ],
  "repository": {
    "type": "git",
    "url": "https://gitee.com/geeksdidi/spring-breeze.git"
  },
  "scripts": {
    "test": "echo \"Error: run tests from root\" && exit 1"
  }
}

```

utils/log/lib/log.js
```
'use strict';
const log = require("npmlog")
log.addLevel("success", 2000, { bg: "green" })
module.exports = log;

```
回到core,修改log为自己的包;并将package.json中dependencies追加@spring-breeze/log;然后执行lerna link

```
 "dependencies": {
    "@spring-breeze/tools": "^0.0.5",
    "import-local": "^3.0.3",
    "jquery": "^3.6.0",
    "lodash": "^4.17.21",
    "npmlog": "^6.0.0",
    "@spring-breeze/log":"^0.0.5"
  },
```

core/下面的bin/index.js:
```
#!/usr/bin/env node
const importLocal = require("import-local")
if(importLocal(__filename)){
    require('@spring-breeze/log').success("cli","正在使用本地的cli")
}else{
    require('../lib/core')()
}
```
