#!/usr/bin/env node
const importLocal = require("import-local")
if(importLocal(__filename)){
    require('@spring-breeze/log').success("cli","正在使用本地的cli")
}else{
    require('../lib/core')()
}